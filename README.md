# Poolway - Distributed process pool


Poolway is a **distributed** pooling library using pg2 and poolboy inspired by kafka.


## Example


```elixir
# In your config/config.exs file

config :poolway, :pools,
  [
    [
      {:channel, :image},
      {:topic, :resize},
      {:size, 10},
      {:mod, {ResizeImage, []}
    ],
    [
      {:channel, :image},
      {:topic, :email},
       {:size: 5},
       {:mod, {Email, []}
    ]
  ]

# In your application code
defmodule ResizeImage do
  use GenServer

  def handle_call(request, from, state) do
    # do something
  end

  def handle_cast(request, state) do
    # do something
  end
end

defmodule Email do
 use GenServer

 def handle_call(request, from, state) do
    # do something
 end

 def handle_cast(request, state) do
    # do something
 end
end

```

## Usage

Add poolway as a dependency in your `mix.exs` file.
```elixir
def deps do
  [{:poolway, git: "https://github.com/ammbot/poolway.git"}]
end
```

You should also update your applications to include projects.
```elixir
def application do
  [applications: [:poolway]]
end
```

You can connect between nodes for scaling horizontally using `Node.connect/1`

## API

call to all services in the channel
```elixir
iex> Poolway.API.call(:image, payload)
```

call to specific topic in channel
```elixir
iex> Poolway.API.call(:image, :resize, payload)
iex> Poolway.API.cast(:image, :email, payload)
```
