defmodule FakeServer do
  use GenServer

  def start_link(opt),
    do: GenServer.start_link __MODULE__, opt

  def init(opt),
    do: {:ok, %{msg: opt[:msg], counter: 0}}

  def handle_call(:get_counter, _from, state),
    do: {:reply, state.counter, state}
  def handle_call(%{body: body}, _from, state) do
    reply = String.reverse body
    {:reply, {state[:msg], reply}, state}
  end

  def handle_cast(integer, state) when is_integer(integer) do
    {:noreply, %{state | counter: state.counter + integer}}
  end

end
