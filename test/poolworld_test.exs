defmodule PoolwayTest do
  use ExUnit.Case

  doctest Poolway
  doctest Poolway.Naming

  setup_all  do
   Code.load_file "fake_server.ex", "test"
    opts = [
      [{:channel, :test}, {:size, 1}, {:mod, {FakeServer, [msg: "ZERO"]}}],
      [{:channel, :test}, {:topic, :one}, {:size, 5}, {:mod, {FakeServer, [msg: "ONE"]}}],
      [{:channel, :test}, {:topic, :two}, {:size, 2}, {:mod, {FakeServer, [msg: "TWO"]}}],
    ]
    Application.put_env :poolway, :pools, opts
    Application.stop :poolway
    Application.ensure_all_started :poolway
    :pg2.start_link
    {:ok, registered: :erlang.registered}
  end

  test "correct register processes", %{registered: registered} do
    assert :'Elixir.Poolway.Supervisor' in registered

    assert :'Elixir.Poolway.Pool.test' in registered
    assert :'Elixir.Poolway.Pool.test.one' in registered
    assert :'Elixir.Poolway.Pool.test.two' in registered

    assert :'Elixir.Poolway.Server.test' in registered
    assert :'Elixir.Poolway.Server.test.one' in registered
    assert :'Elixir.Poolway.Server.test.two' in registered
  end

  test "correct poolboy pool processes", %{registered: registered} do
    assert :poolboy.status(:'Elixir.Poolway.Pool.test') == {:ready, 1, 0, 0}
    assert :poolboy.status(:'Elixir.Poolway.Pool.test.one') == {:ready, 5, 0, 0}
    assert :poolboy.status(:'Elixir.Poolway.Pool.test.two') == {:ready, 2, 0, 0}
  end

  test "correct supervise" do
    mods = :'Elixir.Poolway.Supervisor'
            |> Supervisor.which_children
            |> Enum.map(fn{mod,_,_,_} -> mod end)
    assert length(mods) == 3

    assert :'Elixir.Poolway.Supervisor.test' in mods
    assert :'Elixir.Poolway.Supervisor.test.one' in mods
    assert :'Elixir.Poolway.Supervisor.test.two' in mods

    pids = :'Elixir.Poolway.Supervisor'
            |> Supervisor.which_children
            |> Enum.map(fn{_,pid,_,_} -> pid end)
    assert length(pids) == 3

    mods = Enum.map(pids, &(Supervisor.which_children(&1)))
            |> List.flatten
            |> Enum.map(fn{mod,_,_,_} -> mod end)
    assert :'Elixir.Poolway.Pool.test' in mods
    assert :'Elixir.Poolway.Pool.test.one' in mods
    assert :'Elixir.Poolway.Pool.test.two' in mods
    assert :'Elixir.Poolway.Server.test' in mods
    assert :'Elixir.Poolway.Server.test.one' in mods
    assert :'Elixir.Poolway.Server.test.two' in mods
  end

  test "correct register pg2" do
    assert {:poolway, {:test, nil}} in :pg2.which_groups
    assert {:poolway, {:test, :one}} in :pg2.which_groups
    assert {:poolway, {:test, :two}} in :pg2.which_groups
  end

  test "api call to channel" do
    reply = Poolway.API.call(:test, %{body: "gracias"})
    assert length(reply) == 3
    assert {"ZERO", "saicarg"} in reply
    assert {"ONE", "saicarg"} in reply
    assert {"TWO", "saicarg"} in reply
  end

  test "api call to specific topic" do
    assert Poolway.API.call(:test, :one, %{body: "hola"}) == {"ONE", "aloh"}
    assert Poolway.API.call(:test, :two, %{body: "hello"}) == {"TWO", "olleh"}
  end

  test "api call to unknown channel" do
    reply = Poolway.API.call(:notexist, %{body: "HEY"})
    assert reply == {:error, {:no_such_service, {:poolway, {:notexist, nil}}}}
  end

  test "api call to unknown topic" do
    reply = Poolway.API.call(:test, :notexist, %{body: "HEY"})
    assert reply == {:error, {:no_such_service, {:poolway, {:test, :notexist}}}}
  end

  test "api cast" do
    Poolway.API.cast(:test, :one, 10)
    Poolway.API.cast(:test, :one, 1)
    Poolway.API.cast(:test, :two, 20)
    Poolway.API.cast(:test, :two, 2)
    Poolway.API.cast(:test, 5)

    assert Poolway.API.call(:test, :one, :get_counter) == 16
    assert Poolway.API.call(:test, :two, :get_counter) == 27
    reply = Poolway.API.call(:test, :get_counter)
    assert length(reply) == 3
    assert 16 in reply
    assert 27 in reply
    assert 5 in reply
  end

end
