defmodule Poolway.Server do
  use GenServer

  def start_link(opt),
    do: GenServer.start_link(__MODULE__, opt, [{:name, opt.server_id}])

  def init(opt) do
    Poolway.Naming.pg2(opt.channel, opt.topic)
    |> join_pg2
    {:ok, opt}
  end

  def handle_call(request, _from, state) do
    reply = route(:call, state.pool_id, request)
    {:reply, reply, state}
  end

  def handle_cast(request, state) do
    route(:cast, state.pool_id, request)
    {:noreply, state}
  end

  defp join_pg2(topic) do
    :pg2.create topic
    :pg2.join topic, self
  end

  defp route(fun, poolname, request),
    do: :poolboy.transaction(poolname, &(apply GenServer, fun, [&1, request]))

end
