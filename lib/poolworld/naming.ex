defmodule Poolway.Naming do
  @moduledoc false

  @doc """
  construct module name from module, channel and topic

  ## Examples

      iex> Poolway.Naming.module(Poolway.Server, :test, :one)
      :'Elixir.Poolway.Server.test.one'

      iex> Poolway.Naming.module(Poolway.Pool, :notopic)
      :'Elixir.Poolway.Pool.notopic'

  """
  def module(module, channel, topic \\ nil),
    do: Module.concat [module, channel, topic]

  @doc """
  construct pg2 name from channel and topic

  ## Examples

      iex> Poolway.Naming.pg2(:test, :one)
      {:poolway, {:test, :one}}

      iex> Poolway.Naming.pg2(:notopic)
      {:poolway, {:notopic, nil}}

  """
  def pg2(channel, topic \\ nil),
    do: {:poolway, {channel, topic}}

end
