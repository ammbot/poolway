defmodule Poolway.API do

  alias Poolway.Naming

  @doc """
  call service synchronous in a specific channel.
  request will be broadcast to all topic in the channel
  with randomly in each topic.

  if topic is provided, request will be send directly to that topic.

  if service is not available. It returns
  `{:error, {:no_such_service, {channel, topic}}}`.

  example:

      call(:test, :one, %{body: "hello"})

  """
  def call(channel, topic \\ nil, payload, timeout \\ 5000) do
    route :call, channel, topic, payload, timeout
  end

  @doc """
  cast service asynchronous in a specific channel.
  request will be broadcast to all topic in the channel
  with randomly in each topic.

  if topic is provided, request will be send directly to that topic.

  if service is not available. It returns
  `{:error, {:no_such_service, {channel, topic}}}`.

  example:

      cast(:test, :one, %{body: "hello"})

  """
  def cast(channel, topic \\ nil, payload) do
    route :cast, channel, topic, payload, nil
  end

  defp route(fun, channel, topic, payload, timeout)
       when fun == :call or fun == :cast do
    case get_pid(channel, topic) do
      [] ->
        {:error, {:no_such_service, Naming.pg2(channel)}}
      {:error, {:no_such_group, group}} ->
        {:error, {:no_such_service, group}}
      pid when is_pid(pid) ->
        apply GenServer, fun, [pid, payload]
      pids when is_list(pids) ->
        args = [payload]
        if timeout, do: args = args ++ [timeout]
        Enum.map(pids, &(apply GenServer, fun, [&1] ++ args))
    end
  end

  defp get_pid(channel, nil),
    do: get_pids channel
  defp get_pid(channel, topic) do
    Naming.pg2(channel, topic)
    |> :pg2.get_closest_pid
  end

  defp get_pids(channel) do
    filter = fn{:poolway, {ch, _}} -> ch == channel; _ -> false end
    :pg2.which_groups
    |> Enum.filter_map(filter, &(:pg2.get_closest_pid &1))
  end

end
