defmodule Poolway.Supervisor do
  use Supervisor

  def start_link(spec) do
    Supervisor.start_link __MODULE__, spec
  end

  def init(spec) do
    opts = [strategy: :one_for_all]
    poolboy_child(spec) ++ [worker(Poolway.Server, [spec], id: spec.server_id)]
    |> supervise(opts)
  end

  defp poolboy_child(spec) do
    {mod, args} = spec.mod
    opts = [
      {:name, {:local, spec.pool_id}},
      {:worker_module, mod},
      {:size, spec.size},
      {:max_overflow, div(spec.size, 2)}
    ]
    [:poolboy.child_spec(spec.pool_id, opts, args)]
  end

end
