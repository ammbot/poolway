defmodule Poolway do
  use Application
  import Supervisor.Spec, warn: false
  alias Poolway.Naming

  @sup Poolway.Supervisor

  def start(_type, _args) do
    Application.get_env(:poolway, :pools)
    |> parse_opts
    |> init
  end

  defp init([]),
    do: {:ok, self}
  defp init(specs) do
    opts = [strategy: :one_for_one, name: @sup]
    specs
    |> Enum.map(&(supervisor(@sup, [&1], id: &1.sup_id)))
    |> Supervisor.start_link(opts)
  end

  defp parse_opts(nil),
    do: []
  defp parse_opts([opt|_]=opts) when is_list(opt) do
    sup_f = fn(c,t) -> Naming.module(Poolway.Supervisor, c, t) end
    sv_f = fn(c,t) -> Naming.module(Poolway.Server, c, t) end
    pool_f = fn(c,t) -> Naming.module(Poolway.Pool, c, t) end
    opts
    |> Stream.map(&(Keyword.merge(default_opts, &1)))
    |> Stream.map(&(Enum.into(&1, %{})))
    |> Stream.map(&(Map.put_new(&1, :sup_id, sup_f.(&1.channel, &1.topic))))
    |> Stream.map(&(Map.put_new(&1, :server_id, sv_f.(&1.channel, &1.topic))))
    |> Stream.map(&(Map.put_new(&1, :pool_id, pool_f.(&1.channel, &1.topic))))
    |> Enum.to_list
  end
  defp parse_opts(opts),
    do: parse_opts [opts]

  defp default_opts,
    do: [size: 10, topic: nil]

end
